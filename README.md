# Sermon Manager for WordPress to Sermon Audio

This app downloads sermons from the Sermon Manager for WordPress plugin and imports them to SermonAudio.

[Source code on GitLab](https://gitlab.com/andrewminion/sermon-manager-to-sermon-audio)

# Requirements

- Redis server
- PHP 7.4+ with redis extension
  - Recommended: set the `memory_limit` in `php.ini` to at least `2G` or you may run into `Allowed memory size of X bytes exhausted`. As a workaround, you can run `php -d memory_limit=2G queue:work`, but you’ll lose the benefit of the Horizon monitor
- MySQL
- Recommended: Ngingx or Apache web server (pro-tip: use [Laravel Valet](https://laravel.com/docs/master/valet#introduction))

# Installation

1. Run `git clone` this repository
2. Run `composer install` to install dependencies
3. Run `cp .env.example .env` to create your environment file
4. Edit the `.env` file and add your website and SermonAudio connection information
   - `SERMON_MANAGER_WEBSITE`: the domain name of your website with Sermon Manager for WordPress installed
   - `SERMON_AUDIO_API_KEY`: your SermonAudio API key, found at in the [Members Area](https://www.sermonaudio.com/secure/members_stats.asp)
   - `SERMON_AUDIO_FTP_USERNAME`: your SermonAudio FTP username; enable at [Account > Connections](https://www.sermonaudio.com/dashboard/account/connections/)
   - `SERMON_AUDIO_FTP_PASSWORD`: your SermonAudio FTP password; enable at [Account > Connections](https://www.sermonaudio.com/dashboard/account/connections/)
   - `SERMON_AUDIO_FTP_DIRECTORY`: your SermonAudio FTP upload folder; enable at [Account > Connections](https://www.sermonaudio.com/dashboard/account/connections/)
   - `SERMON_AUDIO_BROADCASTER_ID`: your SermonAudio “username”
5. Run `php artisan key:generate` to generate an app key
6. Run `php artisan migrate` to set up the database

# Usage

1. Run `php horizon &` to run Horizon supervisor in the background (press `fg` to bring to the foreground and then `ctrl-c` to quit it)
2. Run `php artisan schedule:work &` to run scheduled tasks in the background (press `fg` to bring to the foreground and then `ctrl-c` to quit it)
3.  Run `php artisan import:preachers` to import all preachers
4.  Run `php artisan import:sermon-series` to import all sermon series
5.  Run `php artisan import:service-types` to import all service types
6.  Run `php artisan import:sermons` to import all sermons
7.  If you have many preachers, normalize the data:
   1. Look at the `preachers` table in the database to identify duplicate names
   2. To consolidate names, run `php artisan sermons:consolidate-preachers {destinationId} {sourceId ...}` where `{destinationId}` is the preacher you wish too keep and `{sourceId ...}` is one or more preacher IDs to consolidate ino the destination. (example: `php artisan sermons:consolidate-preachers 123 230 345 329`)
   3. Run `php artisan preachers:normalize-names` to remove common ticles (Pastor, Missionary, Evangelist, etc.) to better fit with SermonAudio’s typical naming structure.
   4. Optional: run `php artisan sermons:list-top-preachers` to list the top 25 speakers. You may want to compare these to SermonAudio and update the `preachers` table in your database to match their names for better automatic matching when you upload the sermons.
8.  Run `php artisan sermons:upload-ftp` to upload all sermons via FTP
    - Note: use the `--limit`, `--offset`, `--before`, and/or `--after` parameters to limit the number of sermons uploaded, or use `--id` to upload one specific sermon by database ID
9. Run `php artisan sermons:compare` to compare local database against SermonAudio
10. Run `php artisan sermons:show-missing` to determine what sermons were not uploaded
