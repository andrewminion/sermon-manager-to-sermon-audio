<?php

use App\Models\Preacher;
use App\Models\Sermon;
use App\Models\SermonSeries;
use App\Models\ServiceType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSermonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sermons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sermon_audio_id')->nullable()->default(null);
            $table->boolean('uploaded')->default(false);
            $table->string('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->string('audio_url')->nullable()->default();
            $table->string('video_url')->nullable()->default();
            $table->string('bulletin')->nullable()->default();
            $table->string('duration')->nullable()->default();
            $table->string('bible_passage')->nullable()->default();
            $table->timestamps();
        });

        Schema::create('preacher_sermon', function (Blueprint $table) {
            $table->foreignIdFor(Sermon::class)->constrained();
            $table->foreignIdFor(Preacher::class)->constrained();
        });

        Schema::create('sermon_service_type', function (Blueprint $table) {
            $table->foreignIdFor(Sermon::class)->constrained();
            $table->foreignIdFor(ServiceType::class)->constrained();
        });

        Schema::create('sermon_sermon_series', function (Blueprint $table) {
            $table->foreignIdFor(Sermon::class)->constrained();
            $table->foreignIdFor(SermonSeries::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sermons');
    }
}
