<?php

namespace App\Console\Commands;

use App\Jobs\SermonManager\ImportServiceTypes as ImportAllServiceTypes;
use Illuminate\Console\Command;

class ImportServiceTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:service-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all service types';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! config('import.sermon-manager.domain')) {
            $this->error('You must enter your WordPress domain in the .env file.');
            return 1;
        }

        $batchId = ImportAllServiceTypes::dispatchNow();

        $this->info('Started importing all service types.');
        $this->info('View in Horizon: '.config('app.url').'/'.config('horizon.path').'/batches/'.$batchId);

        return 0;
    }
}
