<?php

namespace App\Console\Commands;

use App\Jobs\SermonManager\ImportSermonSeries as ImportAllSermonSeries;
use Illuminate\Console\Command;

class ImportSermonSeries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sermon-series';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all sermon series';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! config('import.sermon-manager.domain')) {
            $this->error('You must enter your WordPress domain in the .env file.');
            return 1;
        }

        $batchId = ImportAllSermonSeries::dispatchNow();

        $this->info('Started importing all sermon series.');
        $this->info('View in Horizon: '.config('app.url').'/'.config('horizon.path').'/batches/'.$batchId);

        return 0;
    }
}
