<?php

namespace App\Console\Commands;

use App\Jobs\SermonManager\ImportSermons as ImportAllSermons;
use Illuminate\Console\Command;

class ImportSermons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:sermons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all sermons';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! config('import.sermon-manager.domain')) {
            $this->error('You must enter your WordPress domain in the .env file.');
            return 1;
        }

        $batchId = ImportAllSermons::dispatchNow();

        $this->info('Started importing all sermons.');
        $this->info('View in Horizon: '.config('app.url').'/'.config('horizon.path').'/batches/'.$batchId);

        return 0;
    }
}
