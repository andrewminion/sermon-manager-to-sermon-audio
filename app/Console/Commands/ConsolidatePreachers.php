<?php

namespace App\Console\Commands;

use App\Jobs\ConsolidatePreachers as ConsolidatePreachersJob;
use App\Models\Preacher;
use Illuminate\Console\Command;

class ConsolidatePreachers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sermons:consolidate-preachers
                            {destinationId : ID to retain}
                            {sourceId* : One or more IDs to consolidate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consolidate duplicate preachers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $destination = Preacher::find($this->argument('destinationId'));
        $sources = Preacher::whereIn('id', $this->argument('sourceId'))->get();

        ConsolidatePreachersJob::dispatch($destination, $sources);

        return 0;
    }
}
