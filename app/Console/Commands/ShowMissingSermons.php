<?php

namespace App\Console\Commands;

use App\Models\Sermon;
use Illuminate\Console\Command;

class ShowMissingSermons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sermons:show-missing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display sermons that have not been uploaded';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Sermon[] $sermons */
        $sermons = Sermon::whereNull('sermon_audio_id')
            ->select(['id', 'title', 'created_at'])
            ->get();

        $this->table(
            ['ID', 'Title', 'Date'],
            $sermons->toArray()
        );

        $this->info(__(':count sermons have not been uploaded', ['count' => $sermons->count()]));

        return self::SUCCESS;
    }
}
