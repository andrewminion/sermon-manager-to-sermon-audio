<?php

namespace App\Console\Commands;

use App\Models\Sermon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class CompareSermons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sermons:compare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compares local database against SermonAudio to determine what might be missing';

    private Collection $errors;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->errors = collect();

        $domain = 'https://api.sermonaudio.com/';
        $url = 'v2/node/sermons?includeDrafts=true&broadcasterID='.config('import.sermon-audio.broadcaster-id');
        $sermons = collect();

        $firstRequest = Http::withHeaders([
            'x-api-key' => config('import.sermon-audio.api-key'),
        ])
            ->asJson()
            ->get($domain.$url);

        $this->info(__('Collecting information about :count sermons…', ['count' => $firstRequest->json('totalCount')]));
        $bar = $this->output->createProgressBar($firstRequest->json('totalCount')/50);

        do {
            $results = Http::withHeaders([
                'x-api-key' => config('import.sermon-audio.api-key'),
            ])
                ->asJson()
                ->get($domain.$url);
            if ($results->successful()) {
                $sermons = $sermons->merge($results->json('results'));
            }
            $url = $results->json('next');
            $bar->advance();
        } while (! is_null($url));

        $bar->finish();
        $this->newLine();

        $search = [
            '\'' => '%',
            '"' => '%',
            '‘' => '%',
            '’' => '%',
            '“' => '%',
            '”' => '%',
            '-' => '%',
            '–' => '%',
            '—' => '%',
            '.' => '%',
        ];

        $this->info('Processing sermons…');

        $this->withProgressBar($sermons, function ($data) use ($search) {
            $title = str_replace(array_keys($search), array_values($search), $data['fullTitle']);

            $sermon = Sermon::query()
                ->where('title', 'like', $title)
                ->whereDate('created_at', $data['preachDate'])
                ->first();

            if ($sermon) {
                $sermon->sermon_audio_id = $data['sermonID'];
                $sermon->uploaded = true;
                $sermon->save();
            } else {
                $this->errors->push($data);
            }
        });
        $this->newLine();

        if ($this->errors->isNotEmpty()) {
            $this->error('Errors:');
            $this->table(
                ['SermonAudio ID', 'Date', 'Title'],
                $this
                    ->errors
                    ->map(function ($data) {
                        return [
                            'id' => $data['sermonID'],
                            'date' => $data['preachDate'],
                            'title' => $data['fullTitle'],
                        ];
                    })
                    ->toArray(),
            );
        }

        $this->info('Now run `php artisan sermon:show-missing` to see which sermons were not uploaded.');

        return self::SUCCESS;
    }
}
