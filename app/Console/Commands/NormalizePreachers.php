<?php

namespace App\Console\Commands;

use App\Jobs\NormalizePreacherNames;
use Illuminate\Console\Command;

class NormalizePreachers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'preachers:normalize-names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove common titles (Pastor, Missionary, Evangelist, etc.)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        NormalizePreacherNames::dispatch();

        return 0;
    }
}
