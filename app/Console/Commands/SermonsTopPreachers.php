<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SermonsTopPreachers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sermons:list-top-preachers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List the top preakers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $preachers = DB::table('preachers')
            ->join('preacher_sermon', 'preachers.id', '=', 'preacher_sermon.preacher_id')
            ->selectRaw('preachers.name, count(preacher_sermon.sermon_id) as count')
            ->limit(25)
            ->groupBy('preachers.name')
            ->orderByDesc('count')
            ->get();

        $this->table(
            ['Name', 'Count'],
            $preachers->map(function ($row) {
                return (array) $row;
            })
        );

        return 0;
    }
}
