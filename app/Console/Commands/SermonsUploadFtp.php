<?php

namespace App\Console\Commands;

use App\Jobs\SermonAudio\Ftp\UploadAllSermons;
use App\Jobs\SermonAudio\Ftp\UploadSermon;
use App\Models\Sermon;
use Illuminate\Console\Command;

class SermonsUploadFtp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sermons:upload-ftp
                            {--id= : Specific sermon ID}
                            {--limit= : Number of sermons; defaults to all}
                            {--offset= : Number of sermons to skip; defaults to 0}
                            {--after= : Sermons after date (YYYY-MM-DD format)}
                            {--before= : Sermons before date (YYYY-MM-DD format)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload all sermons via FTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (! config('filesystems.disks.sermonaudio.root')) {
            $this->error('You must enter your SermonAudio FTP connection details in the .env file.');
            return self::FAILURE;
        }

        if ($this->option('id')) {
            UploadSermon::dispatch(Sermon::find($this->option('id')));
            $this->info('View in Horizon: '.config('app.url').'/'.config('horizon.path').'/jobs/pending/');
            return self::SUCCESS;
        }

        if ($this->option('limit')) {
            $limit = $this->option('limit');
        } else {
            $limit = null;
        }

        if ($this->option('offset')) {
            $offset = $this->option('offset');
        } else {
            $offset = null;
        }

        if ($this->option('after')) {
            $after = $this->option('after');
        } else {
            $after = null;
        }

        if ($this->option('before')) {
            $before = $this->option('before');
        } else {
            $before = null;
        }

        $batchId = UploadAllSermons::dispatchNow($limit, $offset, $after, $before);

        $this->info('Started uploading sermons.');
        $this->info('View in Horizon: '.config('app.url').'/'.config('horizon.path').'/batches/'.$batchId);


        return self::SUCCESS;
    }
}
