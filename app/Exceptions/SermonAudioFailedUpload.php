<?php

namespace App\Exceptions;

use RuntimeException;

class SermonAudioFailedUpload extends RuntimeException
{
    //
}
