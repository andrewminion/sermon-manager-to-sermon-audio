<?php

namespace App\Jobs;

use App\Models\Preacher;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class NormalizePreacherNames implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $search = array(
            'Assistant Pastor',
            'Dr.',
            'Evangelist',
            'Missionary',
            'Mr.',
            'Pastor',
            'Youth Pastor',
        );
        $replace = array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        );

        Preacher::all()->each(function (Preacher $preacher) use ($search, $replace) {
            $preacher->name = Str::of($preacher->name)->replace($search, $replace)->trim();
            $preacher->save();
        });
    }
}
