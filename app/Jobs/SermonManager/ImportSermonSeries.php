<?php

namespace App\Jobs\SermonManager;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Http;

class ImportSermonSeries implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('imports');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $totalPages = Http::get('https://'.config('import.sermon-manager.domain').'/wp-json/wp/v2/wpfc_sermon_series')->header('x-wp-totalpages');

        $page = 1;
        $jobs = [];
        do {
            $jobs[] = new ImportSermonSeriesBatch($page);
            $page++;
        } while ($page <= $totalPages);

        $batch = Bus::batch($jobs)
            ->name('Import Sermon Series')
            ->onQueue('imports')
            ->dispatch();

        return $batch->id;
    }
}
