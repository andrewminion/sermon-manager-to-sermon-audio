<?php

namespace App\Jobs\SermonManager;

use App\Models\Preacher;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class ImportPreacherBatch implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $page;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($page = 1)
    {
        $this->page = $page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch() && $this->batch()->canceled()) {
            return;
        }

        $response = Http::get('https://'.config('import.sermon-manager.domain').'/wp-json/wp/v2/wpfc_preacher?page='.$this->page);

        $response->collect()->each(function ($preacher) {
            $name = Str::of($preacher['name']);

            (new Preacher([
                'id' => $preacher['id'],
                'name' => $preacher['name'],
                'first_name' => $name->beforeLast(' ')->after(' '),
                'last_name' => $name->afterLast(' '),
                'description' => $preacher['description'],
                'link' => $preacher['link'],
            ]))->save();
        });
    }
}
