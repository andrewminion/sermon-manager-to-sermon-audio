<?php

namespace App\Jobs\SermonManager;

use App\Models\Sermon;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ImportSermonBatch implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $page;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($page = 1)
    {
        $this->page = $page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch() && $this->batch()->canceled()) {
            return;
        }

        $response = Http::get('https://'.config('import.sermon-manager.domain').'/wp-json/wp/v2/wpfc_sermon?page='.$this->page);

        $response->collect()->each(function ($sermon) {
            DB::transaction(function () use ($sermon) {
                $sermonModel = new Sermon([
                    'id' => $sermon['id'],
                    'created_at' => $sermon['sermon_date'],
                    'title' => html_entity_decode($sermon['title']['rendered']),
                    'description' => html_entity_decode($sermon['sermon_description']),
                    'status' => $sermon['status'],
                    'link' => $sermon['link'],
                    'audio_url' => $sermon['sermon_audio'],
                    'video_url' => $sermon['sermon_video_url'],
                    'bulletin' => $sermon['sermon_bulletin'],
                    'duration' => $sermon['sermon_audio_duration'],
                    'bible_passage' => $sermon['bible_passage'],
                ]);

                $sermonModel->save();

                $sermonModel->preachers()->attach($sermon['wpfc_preacher']);
                $sermonModel->sermon_series()->attach($sermon['wpfc_sermon_series']);
                $sermonModel->service_types()->attach($sermon['wpfc_service_type']);
            });
        });
    }
}
