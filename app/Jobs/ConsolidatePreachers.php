<?php

namespace App\Jobs;

use App\Models\Preacher;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ConsolidatePreachers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Preacher */
    private $destination;

    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Preacher[] */
    private $sources;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($destination, $sources)
    {
        $this->destination = $destination;
        $this->sources = $sources;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this
            ->sources
            ->each(function (\App\Models\Preacher $preacher) {
                $preacher
                    ->sermons
                    ->each(function (\App\Models\Sermon $sermon) {
                        $sermon->preachers()->sync([$this->destination->id]);
                    });
            });

        Preacher::destroy($this->sources);
    }
}
