<?php

namespace App\Jobs\SermonAudio\Ftp;

use App\Exceptions\SermonAudioFailedCreate;
use App\Exceptions\SermonAudioFailedUpload;
use getID3;
use getid3_lib;
use getid3_writetags;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class UploadSermon implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Sermon */
    public $sermon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sermon)
    {
        $this->sermon = $sermon;
        $this->onQueue('uploads-ftp');
    }

    /**
     * Execute the job.
     *
     * @return bool
     * @throws SermonAudioFailedCreate
     */
    public function handle()
    {
        if ($this->batch() && $this->batch()->canceled()) {
            return false;
        }

        if ($this->sermon->uploaded) {
            return false;
        }

        // Series.
        if ($this->sermon->sermon_series->isNotEmpty()) {
            $sermonSeries = $this->sermon->sermon_series()->first()->name;
        } else {
            $sermonSeries = null;
        }

        // Service type.
        if ($this->sermon->service_types->isNotEmpty()) {
            $serviceType = $this->sermon->service_types()->first()->name;
        } else {
            $serviceType = config('import.sermon-audio.default-service-type');
        }


        $filename = $this->sermon->created_at->format('Ymd').'-'.$serviceType.'.mp3';

        if (! Storage::disk('local')->exists($filename)) {
            $audio = Http::get($this->sermon->audio_url)->throw()->body();
            Storage::disk('local')->put($filename, $audio);
            unset($audio);
        }

        $getID3 = new getID3();
        $getID3->setOption(array('encoding' => 'UTF-8'));

        getid3_lib::IncludeDependency('vendor/james-heinrich/getid3/getid3/write.php', __FILE__, true);
        $writer = new getid3_writetags();

        $writer->filename = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($filename);
		$writer->overwrite_tags = true;
		$writer->tag_encoding = 'UTF-8';
        $writer->remove_other_tags = true;
        $writer->tagformats = ['id3v2.4'];

        $writer->tag_data = [
            'title' => [$this->sermon->title],
            'artist' => [$this->sermon->preachers->first()->name],
            'album' => [$this->sermon->bible_passage],
            'comment' => [$sermonSeries],
        ];

        if (! $writer->WriteTags()) {
            throw new SermonAudioFailedUpload('Failed to write tags');
        }

        $uploaded = Storage::disk('sermonaudio')->put($filename, Storage::disk('local')->get($filename));

        if (! $uploaded || ! Storage::disk('sermonaudio')->exists($filename)) {
            throw new SermonAudioFailedUpload('Failed to upload to FTP');
        }

        $this->sermon->uploaded = true;
        $this->sermon->save();

        Storage::disk('local')->delete($filename);
        return true;
    }
}
