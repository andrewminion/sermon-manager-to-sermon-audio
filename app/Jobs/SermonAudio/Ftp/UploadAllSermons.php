<?php

namespace App\Jobs\SermonAudio\Ftp;

use App\Models\Sermon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;

class UploadAllSermons implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var int */
    private $limit;

    /** @var int */
    private $offset;

    /** @var string */
    private $after;

    /** @var string */
    private $before;

    /**
     * Create a new job instance.
     *
     * @param int $limit
     * @param int $offset
     * @param string $after
     * @param string $before
     * @return void
     */
    public function __construct($limit = null, $offset = null, $after = null, $before = null)
    {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->after = $after;
        $this->before = $before;
    }

    /**
     * Execute the job.
     *
     * @return int
     */
    public function handle()
    {
        $sermons = Sermon::whereUploaded(false);

        if ($this->after) {
            $sermons->whereDate('created_at', '>', $this->after);
        }

        if ($this->before) {
            $sermons->whereDate('created_at', '<', $this->before);
        }

        if ($this->limit) {
            $sermons->limit($this->limit);
        }

        if ($this->offset) {
            $sermons->skip($this->offset);
        }

        $jobs = $sermons
            ->get()
            ->map(function (Sermon $sermon) {
                return new UploadSermon($sermon);
            });

        $batch = Bus::batch($jobs)
            ->name('Upload sermons via FTP')
            ->onQueue('uploads-ftp')
            ->allowFailures()
            ->dispatch();

        return $batch->id;
    }
}
