<?php

namespace App\Jobs\SermonAudio\Api;

use App\Exceptions\SermonAudioFailedCreate;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;

class CreateSermon implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Sermon */
    public $sermon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sermon)
    {
        $this->sermon = $sermon;
        $this->onQueue('uploads-api');
    }

    /**
     * Execute the job.
     *
     * @return bool
     * @throws SermonAudioFailedCreate
     */
    public function handle()
    {
        if ($this->batch() && $this->batch()->canceled()) {
            return false;
        }

        if ($this->sermon->sermon_audio_id) {
            return false;
        }
// FIXME: determine whether to use API or FTP.

        // Short title.
        $title = Str::of($this->sermon->title);
        if ($title->contains(':')) {
            $shortTitle = $title->before(':');
        } elseif ($title->contains('(')) {
            $shortTitle = $title->before('(');
        } elseif ($title->contains('—')) {
            $shortTitle = $title->before('—');
        } else {
            $shortTitle = $title->limit(29, '…');
        }

        // Series.
        if ($this->sermon->sermon_series->isNotEmpty()) {
            $sermonSeries = $this->sermon->sermon_series()->first()->name;
        } else {
            $sermonSeries = null;
        }

        // Service type.
        if ($this->sermon->service_types->isNotEmpty()) {
            $serviceType = $this->sermon->service_types()->first()->name;
        } else {
            $serviceType = config('import.sermon-audio.default-service-type');
        }

        $newSermon = Http::withHeaders([
            'x-api-key' => config('import.sermon-audio.api-key'),
        ])
            ->post('https://api.sermonaudio.com/v1/broadcaster/create_sermon', [
                'fullTitle' => $this->sermon->title,
                'displayTitle' => $shortTitle,
                'subtitle' => $sermonSeries,
                'speakerName' => $this->sermon->preachers()->first()->name,
                'preachDate' => $this->sermon->created_at->format('Y-m-d'),
                'bibleText' => $this->sermon->bible_passage,
                'moreInfoText' => '',
                'eventType' => $serviceType,
                'languageCode' => config('import.sermon-audio.default-language'),
                'keywords' => '',
                'acceptAdditionalCharges' => false,
            ]);

        if (! $newSermon->json('success')) {
            if ($newSermon->json('message') === 'Invalid speaker. If you have not uploaded any sermons preached by this speaker before, please create this sermon using the website. Subsequent sermons may then be created using this API.') {
                // FIXME: upload via FTP, set uploaded = true but sermonID = null.
                return false;
            } else {
                throw new SermonAudioFailedCreate($newSermon->json('message'), $newSermon->status());
            }
        }

        $this->sermon->sermon_audio_id = $newSermon->json('sermonID');
        $this->sermon->save();

        if ($this->batch()) {
            $this->batch()->add(new UploadSermon($this->sermon));
        } else {
            UploadSermon::dispatch($this->sermon);
        }

        return true;
    }
}
