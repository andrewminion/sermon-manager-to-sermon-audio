<?php

namespace App\Jobs\SermonAudio\Api;

use App\Exceptions\SermonAudioFailedUpload;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class UploadSermon implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var \App\Models\Sermon */
    public $sermon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sermon)
    {
        $this->sermon = $sermon;
        $this->onQueue('uploads-api');
    }

    /**
     * Execute the job.
     *
     * @return bool
     * @throws SermonAudioFailedUpload
     */
    public function handle()
    {
        if ($this->batch() && $this->batch()->canceled()) {
            return false;
        }

        if ($this->sermon->uploaded) {
            return false;
        }

        $audio = Http::get($this->sermon->audio_url)->throw()->body();

        $upload = Http::withHeaders([
            'x-api-key' => config('import.sermon-audio.api-key'),
        ])
            ->post('https://api.sermonaudio.com/v1/broadcaster/upload_audio', [
                'sermonID' => $this->sermon->sermon_audio_id,
                'filename' => basename($this->sermon->audio_url),
                'fileData' => base64_encode($audio),
        ]);

        if ($upload->failed()) {
            try {
                $upload->throw();
            } catch (RequestException $e) {
                throw new SermonAudioFailedUpload($e->getMessage(), $e->getCode());
            }
        }

        if (! $upload->json('success')) {
            throw new SermonAudioFailedUpload($upload->body(), $upload->status());
        }

        $this->sermon->uploaded = true;
        $this->sermon->save();

        return true;
    }
}
