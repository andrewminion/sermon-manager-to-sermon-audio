<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Preacher
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $description
 * @property string|null $wordpress_link
 * @property string|null $sermon_audio_link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sermon[] $sermons
 * @property-read int|null $sermons_count
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher query()
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereSermonAudioLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Preacher whereWordpressLink($value)
 * @mixin \Eloquent
 */
class Preacher extends Model
{
    use HasFactory;

    /**
     * Fillable attributes.
     *
     * @var array $fillable
     * @since 1.0.0
     */
    protected $fillable = [
        'id',
        'description',
        'first_name',
        'last_name',
        'sermonaudio_link',
        'wordpress_link',
        'name',
    ];

    /**
     * Sermons
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Sermon[]
     * @since 1.0.0
     */
    public function sermons()
    {
        return $this->belongsToMany(Sermon::class);
    }
}
