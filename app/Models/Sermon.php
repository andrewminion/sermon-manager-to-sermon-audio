<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Sermon
 *
 * @property int $id
 * @property int|null $sermon_audio_id
 * @property bool $uploaded
 * @property string|null $title
 * @property string|null $description
 * @property string|null $status
 * @property string|null $link
 * @property string|null $audio_url
 * @property string|null $video_url
 * @property string|null $bulletin
 * @property string|null $duration
 * @property string|null $bible_passage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Preacher[] $preachers
 * @property-read int|null $preachers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SermonSeries[] $sermon_series
 * @property-read int|null $sermon_series_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ServiceType[] $service_types
 * @property-read int|null $service_types_count
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereAudioUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereBiblePassage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereBulletin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereSermonAudioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereUploaded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sermon whereVideoUrl($value)
 * @mixin \Eloquent
 */
class Sermon extends Model
{
    use HasFactory;


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'uploaded' => 'boolean',
    ];

    /**
     * Fillable attributes.
     *
     * @var array $fillable
     * @since 1.0.0
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'title',
        'sermon_description',
        'status',
        'link',
        'audio_url',
        'video_url',
        'bulletin',
        'duration',
        'bible_passage',
    ];

    /**
     * Preachers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Preacher[]
     * @since 1.0.0
     */
    public function preachers()
    {
        return $this->belongsToMany(Preacher::class);
    }

    /**
     * SermonSeries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\SermonSeries[]
     * @since 1.0.0
     */
    public function sermon_series()
    {
        return $this->belongsToMany(SermonSeries::class);
    }

    /**
     * ServiceTypes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\ServiceType[]
     * @since 1.0.0
     */
    public function service_types()
    {
        return $this->belongsToMany(ServiceType::class);
    }
}
