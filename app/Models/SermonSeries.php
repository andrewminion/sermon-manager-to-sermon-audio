<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SermonSeries
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $link
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sermon[] $sermons
 * @property-read int|null $sermons_count
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries query()
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SermonSeries whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SermonSeries extends Model
{
    use HasFactory;

    /**
     * Fillable attributes.
     *
     * @var array $fillable
     * @since 1.0.0
     */
    protected $fillable = [
        'id',
        'description',
        'link',
        'name',
    ];

    /**
     * Sermons
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\App\Models\Sermon[]
     * @since 1.0.0
     */
    public function sermons()
    {
        return $this->belongsToMany(Sermon::class);
    }
}
