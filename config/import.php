<?php

return [
    'sermon-manager' => [
        'domain' => env('SERMON_MANAGER_WEBSITE'),
    ],

    'sermon-audio' => [
        'api-key' => env('SERMON_AUDIO_API_KEY'),
        'broadcaster-id' => env('SERMON_AUDIO_BROADCASTER_ID'),

        'default-service-type' => env('SERMON_AUDIO_DEFAULT_EVENT_TYPE', 'Sunday - AM'),
        'default-language' => env('SERMON_AUDIO_DEFAULT_LANGUAGE', 'en'),
    ],
];
